import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:programme_dr_diallo/Programme.dart';

class DetailsProgramme extends StatelessWidget {
  final Programme programme;
  DetailsProgramme({required this.programme}) : super();
  @override
  Widget build(BuildContext context) {
    String dateProgramme = DateFormat("dd-MM-yyyy").format(programme.date!);
    String heureProgramme = programme.heure != null
        ? DateFormat("hh:mm").format(DateTime.parse(programme.heure!))
        : "Indéfinie";
    return Scaffold(
      appBar: AppBar(
        title: Text(programme.libelle!),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            customTexte("Thème"),
            customTexte("${programme.libelle}"),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            customTexte("Lieu"),
            customTexte("${programme.lieu}"),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            customTexte("Date grégorienne"),
            customTexte("${dateProgramme}"),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            customTexte("Date hégirienne"),
            customTexte(HijriCalendar().gregorianToHijri(programme.date!.year,
                programme.date!.month, programme.date!.day)),
            customTexte(HijriCalendar.fromDate(programme.date!).fullDate()),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            customTexte("Heure"),
            customTexte("${heureProgramme}"),
            Padding(padding: EdgeInsets.only(top: 20.0)),
            customTexte("Commentaire"),
            customTexte("${programme.commentaire}")
          ],
        ),
      ),
    );
  }

  Text customTexte(String data, {color: Colors.black, fontSize: 20.0}) {
    return Text(
      data,
      style: TextStyle(color: color, fontSize: fontSize),
    );
  }
}
