import 'package:dio/dio.dart';
import 'package:hijri_picker/hijri_picker.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:programme_dr_diallo/Categorie.dart';
import 'package:programme_dr_diallo/DetailsProgramme.dart';
import 'package:programme_dr_diallo/EditerProgramme.dart';
import 'package:programme_dr_diallo/Programme.dart';
import 'package:programme_dr_diallo/constants.dart';
import 'dart:async';
import 'dart:convert';
import 'package:hijri/hijri_calendar.dart';
import 'package:programme_dr_diallo/fonctions.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class RecherchePage extends StatefulWidget {
  @override
  _RecherchePageSate createState() => _RecherchePageSate();
}

class _RecherchePageSate extends State<RecherchePage> {
  DateTime? dateDebutGregorienne;
  DateTime? dateFinGregorienne;
  String? dateDebutHegirienne;
  String? dateFinHegirienne;
  var selectedDate = new HijriCalendar.now();
  List<Programme> programmes = [];

  TextEditingController controllerDateHegDebut = TextEditingController();
  TextEditingController controllerDateHegFin = TextEditingController();
  TextEditingController controllerDateGregDebut = TextEditingController();
  TextEditingController controllerDateGregFin = TextEditingController();

  @override
  Widget build(BuildContext context) {
    HijriCalendar.setLocal(Localizations.localeOf(context).languageCode);
    return Center(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.45,
                // child: BasicDateField(labelText: 'Date début grégorienne'),
                child: DateTimeField(
                  //initialValue: dateDebutGregorienne,
                  controller: controllerDateGregDebut,
                  format: DateFormat("yyyy-MM-dd"),
                  decoration:
                      InputDecoration(labelText: 'Date début grégorienne'),
                  onChanged: (date) {
                    print('dateDebutGregorienne');
                    dateDebutGregorienne = date;
                    dateDebutHegirienne =
                        new HijriCalendar.fromDate(dateDebutGregorienne!)
                            .toString();
                    print("dateDebutHegirienne " + dateDebutHegirienne!);

                    if (date != null) {
                      controllerDateHegDebut.text = HijriCalendar()
                          .gregorianToHijri(date.year, date.month, date.day);
                    }
                    setState(() {});
                  },
                  onShowPicker: (context, currentValue) {
                    return showDatePicker(
                        context: context,
                        firstDate: DateTime(2020),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100));
                  },
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.02,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.45,
                child: TextFormField(
                  // autofillHints: Iterable.castFrom(source),
                  controller: controllerDateHegDebut,
                  // initialValue: dateDebutHegirienne,
                  decoration:
                      InputDecoration(labelText: 'Date début hégirienne'),
                  onChanged: (date) {
                    print('dateDebutHegirienne');
                    print(dateDebutHegirienne);
                    setState(() {});
                  },
                  onTap: () {
                    _selectDateDebut(context);
                  },
                ),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.45,
                child: DateTimeField(
                  controller: controllerDateGregFin,
                  //initialValue: dateFinGregorienne,
                  format: DateFormat("yyyy-MM-dd"),
                  decoration:
                      InputDecoration(labelText: 'Date fin grégorienne'),
                  onChanged: (date) {
                    print('dateFinGregorienne');
                    dateFinGregorienne = date;
                    if (date != null) {
                      controllerDateHegFin.text = HijriCalendar()
                          .gregorianToHijri(date.year, date.month, date.day);
                    }
                    setState(() {});
                  },
                  onShowPicker: (context, currentValue) {
                    return showDatePicker(
                        context: context,
                        firstDate: DateTime(2020),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100));
                  },
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.02,
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.45,
                child: TextFormField(
                  //initialValue: dateFinHegirienne.toString(),
                  controller: controllerDateHegFin,
                  decoration: InputDecoration(labelText: 'Date fin hégirienne'),
                  onChanged: (date) {
                    print('dateDebutHegirienne');
                    dateFinHegirienne = date;
                    setState(() {});
                  },
                  onTap: () {
                    _selectDateFin(context);
                  },
                ),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          ElevatedButton(
              onPressed: () {
                print(
                    "rechercheDeProgrammes lancééééééééééééééééééééééééééééééé");
                print(controllerDateGregDebut.text);
                print(controllerDateGregFin.text);
                //rechercheDeProgrammes(dateDebutGregorienne, dateFinGregorienne);
                rechercheDeProgrammes();
              },
              child: Text('Recherche')),
          Expanded(
            child: ListView.separated(
                itemBuilder: (context, index) {
                  var value = programmes[index];
                  String dateActionToString =
                      DateFormat("dd-MM-yyyy").format(value.date!);
                  return ListTile(
                    onTap: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (BuildContext context) {
                        return DetailsProgramme(
                          programme: value,
                        );
                      }));
                    },
                    title: Text(
                      value.libelle ?? "",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    trailing: Wrap(
                      // mainAxisSize: MainAxisSize.min,
                      children: [
                        IconButton(
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(
                                  builder: (BuildContext buildContext) {
                                return EditerProgrammePage(programme: value);
                              }));
                            },
                            icon: Icon(Icons.edit)),
                        IconButton(
                          onPressed: () {
                            alert(value.id!);
                          },
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                      ],
                    ),
                    subtitle: Row(
                      children: [
                        Text((value.categorie?.libelle ?? "") + " le ",
                            style: TextStyle(fontStyle: FontStyle.italic)),
                        Text(
                          dateActionToString + " => ",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Text(HijriCalendar().gregorianToHijri(value.date!.year,
                            value.date!.month, value.date!.day))
                      ],
                    ),
                    selected: true,
                  );
                },
                separatorBuilder: (context, index) {
                  return Divider(
                    height: 2,
                    color: Colors.teal[700],
                    endIndent: 20,
                    indent: 20,
                  );
                },
                itemCount: programmes.length),
          )
        ],
      ),
    ));
  }

  Future<Null> _selectDateDebut(BuildContext context) async {
    final HijriCalendar? picked = await showHijriDatePicker(
      context: context,
      initialDate: selectedDate,
      lastDate: new HijriCalendar()
        ..hYear = 1500
        ..hMonth = 9
        ..hDay = 25,
      firstDate: new HijriCalendar()
        ..hYear = 1438
        ..hMonth = 12
        ..hDay = 25,
      initialDatePickerMode: DatePickerMode.day,
    );
    if (picked != null)
      setState(() {
        selectedDate = picked;
        controllerDateHegDebut.text = selectedDate.toString();
        controllerDateGregDebut.text = DateFormat("yyyy-MM-dd").format(
            HijriCalendar().hijriToGregorian(
                selectedDate.hYear, selectedDate.hMonth, selectedDate.hDay));
        // "${selectedDate.hYear}-${selectedDate.hMonth}-${selectedDate.hDay}";
      });
  }

  Future<Null> _selectDateFin(BuildContext context) async {
    final HijriCalendar? picked = await showHijriDatePicker(
      context: context,
      initialDate: selectedDate,
      lastDate: new HijriCalendar()
        ..hYear = 1500
        ..hMonth = 9
        ..hDay = 25,
      firstDate: new HijriCalendar()
        ..hYear = 1438
        ..hMonth = 12
        ..hDay = 25,
      initialDatePickerMode: DatePickerMode.day,
    );
    if (picked != null)
      setState(() {
        selectedDate = picked;
        controllerDateHegFin.text = selectedDate.toString();
        controllerDateGregFin.text = DateFormat("yyyy-MM-dd").format(
            HijriCalendar().hijriToGregorian(
                selectedDate.hYear, selectedDate.hMonth, selectedDate.hDay));
        //"${selectedDate.hYear}-${selectedDate.hMonth}-${selectedDate.hDay}";
      });
  }

  rechercheDeProgrammes() async {
    var dio = Dio();
    List<Programme> listeProgrammes = [];
    // String debut = DateFormat("yyyy-MM-dd").format(dateDebutGregorienne!);
    // String fin = dateFinGregorienne == null
    //     ? debut
    //     : DateFormat("yyyy-MM-dd").format(dateFinGregorienne!);

    String debut = controllerDateGregDebut.text;
    String fin = controllerDateGregFin.text;

    try {
      var response = await dio.get(
          "${basicUrl}programmes-entre-deux-dates/${debut}/${fin}"); // il est mieux de mettre valeur dans un fichier constant ca evite les difficultes d'edition et les erreurs de saisie
      print(response);
      var itemsResponse = jsonDecode(response.data)["hydra:member"];
      for (var item in itemsResponse) {
        var prog = Map<String, dynamic>.from(item);
        print(item);
        listeProgrammes.add(Programme(
            id: item["id"],
            categorie: Categorie(
                id: item["categorie"]["id"],
                libelle: item["categorie"]["libelle"]),
            date: DateTime.parse(item["date"]),
            lieu: item["lieu"],
            libelle: item["libelle"],
            commentaire: item["commentaire"],
            isDeleted: item["isDeleted"],
            heure: item["heure"]));
      }
      programmes = listeProgrammes;
      print('goooooog');
    } catch (e) {
      print(e);
    }
    this.setState(() {});
  }

  Future<Null> alert(int programmeId) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Confirmation de la suppression d\'un programme'),
            content: Text(
                'Etes-vous vraiment sûrs de bien vouloir supprimer ce programme ? Cette action est irreversible.'),
            contentPadding: EdgeInsets.all(20.0),
            actions: [
              TextButton(
                  onPressed: () {
                    print("Action annulée ${programmeId}");
                    Navigator.pop(context);
                  },
                  child: Text('Annuler',
                      style: TextStyle(color: Colors.grey[600]))),
              TextButton(
                onPressed: () {
                  print("Action acceptée ${programmeId}");
                  Navigator.pop(context);
                  deleteProgramme(programmeId);
                  setState(() {});
                },
                child: Text(
                  'Continuer',
                  style: TextStyle(color: Colors.red[500]),
                ),
              )
            ],
          );
        },
        barrierDismissible: false);
  }

  deleteProgramme(programmeId) async {
    print('deleteProgramme');
    var dio = Dio();
    try {
      var response = await dio.delete(
          "${basicUrl}programmes/${programmeId}"); // il est mieux de mettre valeur dans un fichier constant ca evite les difficultes d'edition et les erreurs de saisie
      print(
          "Retour suppression =======================================================");
      //print(response);
      print(response.statusCode);
      if (response.statusCode == 204) {
        //rechercheDeProgrammes(dateDebutGregorienne, dateFinGregorienne);
        rechercheDeProgrammes();
      }
    } catch (e) {
      print(e);
    }
  }
}

Future<HijriCalendar?> showHijriDatePicker({
  required BuildContext context,
  required HijriCalendar initialDate,
  required HijriCalendar firstDate,
  required HijriCalendar lastDate,
  selectableDayPredicate,
  DatePickerMode initialDatePickerMode: DatePickerMode.day,
  Locale? locale,
  // TextDirection? textDirection,
}) async {
  assert(
      !initialDate.isBefore(firstDate.hYear, firstDate.hMonth, firstDate.hDay),
      'initialDate must be on or after firstDate');
  assert(!initialDate.isAfter(lastDate.hYear, lastDate.hMonth, lastDate.hDay),
      'initialDate must be on or before lastDate');
  assert(!firstDate.isAfter(lastDate.hYear, lastDate.hMonth, lastDate.hDay),
      'lastDate must be on or after firstDate');
  assert(selectableDayPredicate == null || selectableDayPredicate(initialDate),
      'Provided initialDate must satisfy provided selectableDayPredicate');

  Widget child = new HijriDatePickerDialog(
    initialDate: initialDate,
    firstDate: firstDate,
    lastDate: lastDate,
    selectableDayPredicate: selectableDayPredicate,
    initialDatePickerMode: initialDatePickerMode,
  );

  // if (textDirection != null) {
  //   child = new Directionality(
  //     textDirection: textDirection,
  //     child: child,
  //   );
  // }

  if (locale != null) {
    child = new Localizations.override(
      context: context,
      locale: locale,
      child: child,
    );
  }

  return showDialog<HijriCalendar>(
    context: context,
    builder: (BuildContext context) => child,
  );
}
