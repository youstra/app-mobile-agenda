import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:hijri_picker/hijri_picker.dart';
import 'package:programme_dr_diallo/Categorie.dart';
import 'package:programme_dr_diallo/HomePage.dart';
import 'package:programme_dr_diallo/constants.dart';
import 'package:programme_dr_diallo/fonctions.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:programme_dr_diallo/main.dart';

class AjoutProgrammePage extends StatefulWidget {
  @override
  _AjoutProgrammePageSate createState() => _AjoutProgrammePageSate();
}

class _AjoutProgrammePageSate extends State<AjoutProgrammePage> {
  int? categorieId;
  String? date;
  String? lieu;
  String? libelle;
  String? commentaire;
  TimeOfDay? heure;
  List<Categorie> categories = [];
  String dropdownValue = 'One';

  TextEditingController controllerDateHegDebut = TextEditingController();
  TextEditingController controllerDateGregDebut = TextEditingController();
  TextEditingController controllerHeureProgramme = TextEditingController();

  var selectedDate = new HijriCalendar.now();
  DateTime? dateDebutGregorienne;
  String? dateDebutHegirienne;

  @override
  void initState() {
    super.initState();
    getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
              child: ListView(
            children: [
              Text(
                'Ajouter un nouveau programme',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.teal[700],
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold),
              ),
              DropdownButtonFormField(
                  value: categorieId,
                  onChanged: (int? newValue) {
                    setState(() {
                      categorieId = newValue!;
                    });
                  },
                  items:
                      categories.map<DropdownMenuItem<int>>((Categorie value) {
                    return DropdownMenuItem<int>(
                      value: value.id!,
                      child: Text(value.libelle!),
                    );
                  }).toList()),
              TextFormField(
                initialValue: libelle,
                onChanged: (String string) {
                  libelle = string;
                },
                decoration: InputDecoration(labelText: "Libelle"),
              ),
              TextFormField(
                initialValue: lieu,
                onChanged: (String string) {
                  lieu = string;
                },
                decoration: InputDecoration(labelText: "Lieu"),
              ),
              DateTimeField(
                controller: controllerDateGregDebut,
                format: DateFormat("yyyy-MM-dd"),
                decoration: InputDecoration(labelText: 'Date grégorienne'),
                onChanged: (date) {
                  dateDebutGregorienne = date;
                  if (date != null) {
                    controllerDateHegDebut.text = HijriCalendar()
                        .gregorianToHijri(date.year, date.month, date.day);
                  }
                  setState(() {});
                },
                onShowPicker: (context, currentValue) {
                  return showDatePicker(
                      context: context,
                      firstDate: DateTime(2020),
                      initialDate: currentValue ?? DateTime.now(),
                      lastDate: DateTime(2100));
                },
              ),
              TextFormField(
                controller: controllerDateHegDebut,
                decoration: InputDecoration(labelText: 'Date hégirienne'),
                onChanged: (date) {
                  dateDebutHegirienne = date;
                  setState(() {});
                },
                onTap: () {
                  _selectDateDebut(context);
                },
              ),
              TextFormField(
                // initialValue: heure,
                controller: controllerHeureProgramme,
                // onChanged: (String string) {
                //   setState(() {});
                // },
                onTap: montrerTimePicker,
                decoration: InputDecoration(labelText: "Heure"),
              ),
              // ElevatedButton(
              //     onPressed: montrerTimePicker,
              //     child: Text((heure == null ? "Appuyer moi" : heure))),
              TextFormField(
                initialValue: commentaire,
                maxLines: 10,
                keyboardType: TextInputType.multiline,
                onChanged: (String string) {
                  setState(() {
                    commentaire = string;
                  });
                },
                decoration: InputDecoration(labelText: "Commentaire"),
              ),
              Padding(padding: EdgeInsets.only(top: 20.0)),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(),
                  onPressed: saveProgramme,
                  child: Text("Enregistrer"))
            ],
          )),
        ],
      ),
    ));
  }

  void saveProgramme() async {
    Map programmeAEnregistrer = {
      "categorie": "/api/categories/${categorieId}",
      "date": controllerDateGregDebut.text,
      "lieu": lieu,
      "libelle": libelle,
      // "heure": controllerHeureProgramme.text,
      "commentaire": commentaire
    };

    print(programmeAEnregistrer);
    if (categorieId == null ||
        programmeAEnregistrer["date"] == null ||
        libelle == null) {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return SimpleDialog(
              title: Text("Erreur"),
              contentPadding: EdgeInsets.all(15.0),
              children: [
                Text(
                    "Veuillez remplir les champs catégorie, date et libelle avant de continuer"),
                ElevatedButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text("OK"))
              ],
            );
          });
    } else {
      var dio = Dio();
      try {
        var response = await dio.post("${basicUrl}programmes",
            data: programmeAEnregistrer);
        // var itemsResponse = jsonDecode(response.data)["hydra:member"];
        if (response.statusCode == 201) {
          snack("${libelle} enregistré avec succès", context);
          resetAndOpenPage(context: context);
        }
      } catch (e) {
        print(e);
        snack("Une erreur est survenue", context);
      }
    }
  }

  getCategories() async {
    var dio = Dio();
    try {
      var response = await dio.get("${basicUrl}categories");
      var itemsResponse = jsonDecode(response.data)["hydra:member"];
      for (var item in itemsResponse) {
        categories.add(Categorie(id: item["id"], libelle: item["libelle"]));
      }
    } catch (e) {
      print(e);
    }
    this.setState(() {});
  }

  static resetAndOpenPage({required BuildContext context, dynamic view}) {
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(
            builder: (BuildContext context) => view != null
                ? view
                : MyHomePage(
                    title: "Agenda Dr DIALLO",
                  )),
        (Route<dynamic> route) => false);
  }

  Future<Null> montrerTimePicker() async {
    TimeOfDay? time =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    if (time != null) {
      setState(() {
        controllerHeureProgramme.text = time.toString();
      });
    }
  }

  Future<Null> dialog(String title, String desc) async {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text(
              title,
              textScaleFactor: 1.4,
            ),
            contentPadding: EdgeInsets.all(15.0),
            children: [
              Text(desc),
              ElevatedButton(
                  // style: ButtonStyle(backgroundColor: MaterialStateColor(15)),
                  onPressed: () {
                    print('Appuyé');
                    Navigator.pop(context);
                  },
                  child: Text('OK'))
            ],
          );
        });
  }

  Future<Null> _selectDateDebut(BuildContext context) async {
    final HijriCalendar? picked = await showHijriDatePicker(
      context: context,
      initialDate: selectedDate,
      lastDate: new HijriCalendar()
        ..hYear = 1500
        ..hMonth = 9
        ..hDay = 25,
      firstDate: new HijriCalendar()
        ..hYear = 1438
        ..hMonth = 12
        ..hDay = 25,
      initialDatePickerMode: DatePickerMode.day,
    );
    if (picked != null)
      setState(() {
        selectedDate = picked;
        controllerDateHegDebut.text = selectedDate.toString();
        controllerDateGregDebut.text = DateFormat("yyyy-MM-dd").format(
            HijriCalendar().hijriToGregorian(
                selectedDate.hYear, selectedDate.hMonth, selectedDate.hDay));
        // "${selectedDate.hYear}-${selectedDate.hMonth}-${selectedDate.hDay}";
      });
  }
}

Future<HijriCalendar?> showHijriDatePicker({
  required BuildContext context,
  required HijriCalendar initialDate,
  required HijriCalendar firstDate,
  required HijriCalendar lastDate,
  selectableDayPredicate,
  DatePickerMode initialDatePickerMode: DatePickerMode.day,
  Locale? locale,
  // TextDirection? textDirection,
}) async {
  assert(
      !initialDate.isBefore(firstDate.hYear, firstDate.hMonth, firstDate.hDay),
      'initialDate must be on or after firstDate');
  assert(!initialDate.isAfter(lastDate.hYear, lastDate.hMonth, lastDate.hDay),
      'initialDate must be on or before lastDate');
  assert(!firstDate.isAfter(lastDate.hYear, lastDate.hMonth, lastDate.hDay),
      'lastDate must be on or after firstDate');
  assert(selectableDayPredicate == null || selectableDayPredicate(initialDate),
      'Provided initialDate must satisfy provided selectableDayPredicate');

  Widget child = new HijriDatePickerDialog(
    initialDate: initialDate,
    firstDate: firstDate,
    lastDate: lastDate,
    selectableDayPredicate: selectableDayPredicate,
    initialDatePickerMode: initialDatePickerMode,
  );

  // if (textDirection != null) {
  //   child = new Directionality(
  //     textDirection: textDirection,
  //     child: child,
  //   );
  // }

  if (locale != null) {
    child = new Localizations.override(
      context: context,
      locale: locale,
      child: child,
    );
  }

  return showDialog<HijriCalendar>(
    context: context,
    builder: (BuildContext context) => child,
  );

  // getCategories() async {
  //   var dio = Dio();
  //   try {
  //     var response = await dio
  //         // .get('http://127.0.0.1:8000/api/dix-premiers-programmes-a-venir');
  //         .get(
  //             "${basicUrl}dix-premiers-programmes-a-venir"); // il est mieux de mettre valeur dans un fichier constant ca evite les difficultes d'edition et les erreurs de saisie
  //     var itemsResponse = jsonDecode(response.data)["hydra:member"];
  //     for (var item in itemsResponse) {
  //       // var prog = Map<String, dynamic>.from(item);
  //       print(item["categorie"]["libelle"]);
  //       listeProgrammes.add(Programme(
  //           id: item["id"],
  //           categorie: Categorie(
  //               id: item["categorie"]["id"],
  //               libelle: item["categorie"]["libelle"]),
  //           date: DateTime.parse(item["date"]),
  //           lieu: item["lieu"],
  //           libelle: item["libelle"],
  //           commentaire: item["commentaire"],
  //           isDeleted: item["isDeleted"],
  //           heure: item["heure"]));
  //     }
  //     print('goooooog');
  //   } catch (e) {
  //     print(e);
  //   }
  //   this.setState(() {});
  //   //return programmesToReturn;
  // }
}
