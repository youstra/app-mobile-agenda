import 'package:flutter/material.dart';

class Categorie {
  int? id;
  String? libelle;

  Categorie({this.id, this.libelle});
}
