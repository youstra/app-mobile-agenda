import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:programme_dr_diallo/AjoutProgramme.dart';
import 'package:programme_dr_diallo/Categorie.dart';
import 'package:programme_dr_diallo/HomePage.dart';
import 'package:programme_dr_diallo/Programme.dart';
import 'package:programme_dr_diallo/Recherche.dart';
import 'package:programme_dr_diallo/constants.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:hijri_picker/hijri_picker.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mon Agenda',
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        //     const Locale('en', 'USA'),
        const Locale('ar', 'SA'),
      ],
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: MyHomePage(title: 'Agenda Dr DIALLO'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int selectedIndex = 0;
  late PageController _c;

  @override
  void initState() {
    _c = new PageController(
      initialPage: selectedIndex,
    );
    super.initState();
    // getDixPremiersProgrammesAVenirNew();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: this.getBody(),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 30.0,
        mouseCursor: MouseCursor.defer,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.list), label: "Programmes"),
          BottomNavigationBarItem(icon: Icon(Icons.search), label: "Recherche"),
          BottomNavigationBarItem(
              icon: Icon(Icons.add), label: "Ajouter Programme")
        ],
        onTap: (int index) {
          print("Index courant === ${index}");
          this._c.animateToPage(index,
              duration: const Duration(milliseconds: 500),
              curve: Curves.easeInOut);
          this.onTapHandler(index);
        },
        type: BottomNavigationBarType.fixed,
        currentIndex: this.selectedIndex,
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget getBody() {
    return PageView(
        controller: _c,
        onPageChanged: (index) {
          setState(() {
            this.selectedIndex = index;
          });
        },
        children: <Widget>[HomePage(), RecherchePage(), AjoutProgrammePage()]);
    // pas besoin de defnir les widgets avant de les utiliser comme des variables tu peux les utliser directement comme je l'ai fait ici
    // if (this.selectedIndex == 0) {
    //   return HomePage(programmes: listeProgrammes);
    // } else if (this.selectedIndex == 1) {
    //   return RecherchePage();
    // } else {
    //   return AjoutProgramme();
    // }
  }

  void onTapHandler(int index) {
    this.setState(() {
      this.selectedIndex = index;
    });
  }

  // getDixPremiersProgrammesAVenirNew() async {
  //   print("getDixPremiersProgrammesAVenirNew");
  //   var dio = Dio();
  //   try {
  //     var response = await dio
  //         // .get('http://127.0.0.1:8000/api/dix-premiers-programmes-a-venir');
  //         .get(
  //             "${basicUrl}dix-premiers-programmes-a-venir"); // il est mieux de mettre valeur dans un fichier constant ca evite les difficultes d'edition et les erreurs de saisie
  //     var itemsResponse = jsonDecode(response.data)["hydra:member"];
  //     for (var item in itemsResponse) {
  //       // var prog = Map<String, dynamic>.from(item);
  //       print(item["categorie"]["libelle"]);
  //       listeProgrammes.add(Programme(
  //           id: item["id"],
  //           categorie: Categorie(
  //               id: item["categorie"]["id"],
  //               libelle: item["categorie"]["libelle"]),
  //           date: DateTime.parse(item["date"]),
  //           lieu: item["lieu"],
  //           libelle: item["libelle"],
  //           commentaire: item["commentaire"],
  //           isDeleted: item["isDeleted"],
  //           heure: item["heure"]));
  //     }
  //     print('goooooog');
  //   } catch (e) {
  //     print(e);
  //   }
  //   this.setState(() {});
  // }
}
