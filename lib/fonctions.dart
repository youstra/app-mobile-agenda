import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:programme_dr_diallo/constants.dart';
import 'package:programme_dr_diallo/Categorie.dart';
import 'package:programme_dr_diallo/Programme.dart';

Future<List<Programme>> getDixPremiersProgrammesAVenir() async {
  print("getDixPremiersProgrammesAVenirNew");
  List<Programme> listeProgrammes = [];
  var dio = Dio();
  try {
    var response = await dio.get("${basicUrl}dix-premiers-programmes-a-venir");
    var itemsResponse = jsonDecode(response.data)["hydra:member"];
    for (var item in itemsResponse) {
      // var prog = Map<String, dynamic>.from(item);
      print(item["categorie"]["libelle"]);
      listeProgrammes.add(Programme(
          id: item["id"],
          categorie: Categorie(
              id: item["categorie"]["id"],
              libelle: item["categorie"]["libelle"]),
          date: DateTime.parse(item["date"]),
          lieu: item["lieu"],
          libelle: item["libelle"],
          commentaire: item["commentaire"],
          isDeleted: item["isDeleted"],
          heure: item["heure"]));
    }
  } catch (e) {
    print(e);
  }
  return listeProgrammes;
}

void snack(String content, BuildContext context) {
  SnackBar snackBar = SnackBar(content: Text(content));
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
