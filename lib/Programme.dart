import 'package:flutter/material.dart';
import 'package:programme_dr_diallo/Categorie.dart';
import 'package:json_annotation/json_annotation.dart';

// part 'example.g.dart';

// @JsonSerializable()
class Programme {
  int? id;
  Categorie? categorie;
  DateTime? date;
  String? lieu;
  String? libelle;
  String? commentaire;
  bool? isDeleted;
  String? heure;

  Programme(
      {this.id,
      this.categorie,
      this.date,
      this.lieu,
      this.libelle,
      this.commentaire,
      this.isDeleted,
      this.heure});

  // factory Programme.fromJson(Map<String, dynamic> json) =>
  //     _$ProgrammeFromJson(json);

  // Map<String, dynamic> toJson() => _$ProgrammeToJson(this);
}
