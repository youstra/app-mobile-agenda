import 'dart:async';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:hijri_picker/hijri_picker.dart';
import 'package:programme_dr_diallo/Categorie.dart';
import 'package:programme_dr_diallo/HomePage.dart';
import 'package:programme_dr_diallo/Programme.dart';
import 'package:programme_dr_diallo/constants.dart';
import 'package:programme_dr_diallo/fonctions.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';

class EditerProgrammePage extends StatefulWidget {
  EditerProgrammePage({required this.programme});
  Programme? programme;
  @override
  _EditerProgrammePageSate createState() => _EditerProgrammePageSate();
}

class _EditerProgrammePageSate extends State<EditerProgrammePage> {
  int? categorieId;

  TextEditingController controllerDateHegDebut = TextEditingController();
  TextEditingController controllerDateGregDebut = TextEditingController();
  TextEditingController controllerHeureProgramme = TextEditingController();

  var selectedDate = new HijriCalendar.now();
  DateTime? dateDebutGregorienne;
  String? dateDebutHegirienne;
  List<Categorie> categories = [];
  List<Programme> listeProgrammes = [];

  @override
  void initState() {
    super.initState();
    categorieId = widget.programme!.categorie!.id;
    controllerDateGregDebut.text =
        DateFormat("dd-MM-yyyy").format(widget.programme!.date!);
    controllerDateHegDebut.text = HijriCalendar().gregorianToHijri(
        widget.programme!.date!.year,
        widget.programme!.date!.month,
        widget.programme!.date!.day);
    getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edition de ${widget.programme!.libelle}"),
      ),
      body: Center(
          child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Expanded(
                child: ListView(
              children: [
                Text(
                  'Editer un programme',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.teal[700],
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                ),
                DropdownButtonFormField(
                    value: widget.programme!.categorie!.id,
                    onChanged: (int? newValue) {
                      setState(() {
                        categorieId = newValue!;
                      });
                    },
                    items: categories
                        .map<DropdownMenuItem<int>>((Categorie value) {
                      return DropdownMenuItem<int>(
                        value: value.id!,
                        child: Text(value.libelle!),
                      );
                    }).toList()),
                TextFormField(
                  initialValue: widget.programme!.libelle,
                  onChanged: (String string) {
                    widget.programme!.libelle = string;
                  },
                  decoration: InputDecoration(labelText: "Libelle"),
                ),
                TextFormField(
                  initialValue: widget.programme!.lieu,
                  onChanged: (String string) {
                    widget.programme!.lieu = string;
                  },
                  decoration: InputDecoration(labelText: "Lieu"),
                ),
                DateTimeField(
                  controller: controllerDateGregDebut,
                  format: DateFormat("yyyy-MM-dd"),
                  decoration: InputDecoration(labelText: 'Date grégorienne'),
                  onChanged: (date) {
                    dateDebutGregorienne = date;
                    if (date != null) {
                      controllerDateHegDebut.text = HijriCalendar()
                          .gregorianToHijri(date.year, date.month, date.day);
                    }
                    setState(() {});
                  },
                  onShowPicker: (context, currentValue) {
                    return showDatePicker(
                        context: context,
                        firstDate: DateTime(2020),
                        initialDate: currentValue ?? DateTime.now(),
                        lastDate: DateTime(2100));
                  },
                ),
                TextFormField(
                  controller: controllerDateHegDebut,
                  decoration: InputDecoration(labelText: 'Date hégirienne'),
                  onChanged: (date) {
                    dateDebutHegirienne = date;
                    setState(() {});
                  },
                  onTap: () {
                    _selectDateDebut(context);
                  },
                ),
                TextFormField(
                  // initialValue: heure,
                  controller: controllerHeureProgramme,
                  // onChanged: (String string) {
                  //   setState(() {});
                  // },
                  onTap: montrerTimePicker,
                  decoration: InputDecoration(labelText: "Heure"),
                ),
                // ElevatedButton(
                //     onPressed: montrerTimePicker,
                //     child: Text((heure == null ? "Appuyer moi" : heure))),
                TextFormField(
                  initialValue: widget.programme!.commentaire,
                  maxLines: 10,
                  keyboardType: TextInputType.multiline,
                  onChanged: (String string) {
                    setState(() {
                      widget.programme!.commentaire = string;
                    });
                  },
                  decoration: InputDecoration(labelText: "Commentaire"),
                ),
                Padding(padding: EdgeInsets.only(top: 20.0)),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(),
                    onPressed: saveProgramme,
                    child: Text("Mettre à jour"))
              ],
            )),
          ],
        ),
      )),
    );
  }

  void saveProgramme() async {
    Map programmeAEnregistrer = {
      "categorie": "/api/categories/${categorieId}",
      "date": controllerDateGregDebut.text,
      "lieu": widget.programme!.lieu,
      "libelle": widget.programme!.libelle,
      // "heure": controllerHeureProgramme.text,
      "commentaire": widget.programme!.commentaire
    };

    print(programmeAEnregistrer);
    var dio = Dio();
    try {
      var response = await dio.patch(
          "${basicUrl}programmes/${widget.programme!.id}",
          data: programmeAEnregistrer);
      // var itemsResponse = jsonDecode(response.data)["hydra:member"];
      if (response.statusCode == 200) {
        // dialog("Succès", "${widget.programme!.libelle} mis à jour avec succès.");
        snack("${widget.programme!.libelle} mis à jour avec succès.", context);
        Navigator.pop(context);
        // Navigator.push(context,
        //     MaterialPageRoute(builder: (BuildContext context) {
        //   return HomePage();
        // }));
        // Navigator.push(context,
        //     MaterialPageRoute(builder: (BuildContext context) {
        //   setState(() {
        //     listeProgrammes =
        //         getDixPremiersProgrammesAVenir() as List<Programme>;
        //   });
        //   return HomePage(programmes: listeProgrammes);
        // }));
      }
    } catch (e) {
      print(e);
    }
  }

  getCategories() async {
    var dio = Dio();
    try {
      var response = await dio.get("${basicUrl}categories");
      var itemsResponse = jsonDecode(response.data)["hydra:member"];
      for (var item in itemsResponse) {
        categories.add(Categorie(id: item["id"], libelle: item["libelle"]));
      }
    } catch (e) {
      print(e);
    }
    this.setState(() {});
  }

  Future<Null> montrerTimePicker() async {
    TimeOfDay? time =
        await showTimePicker(context: context, initialTime: TimeOfDay.now());
    if (time != null) {
      setState(() {
        controllerHeureProgramme.text = time.toString();
      });
    }
  }

  Future<Null> dialog(String title, String desc) async {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context) {
          return SimpleDialog(
            title: Text(
              title,
              textScaleFactor: 1.4,
            ),
            contentPadding: EdgeInsets.all(15.0),
            children: [
              Text(desc),
              ElevatedButton(
                  // style: ButtonStyle(backgroundColor: MaterialStateColor(15)),
                  onPressed: () {
                    print('Appuyé');
                    Navigator.pop(context);
                  },
                  child: Text('OK'))
            ],
          );
        });
  }

  Future<Null> _selectDateDebut(BuildContext context) async {
    final HijriCalendar? picked = await showHijriDatePicker(
      context: context,
      initialDate: selectedDate,
      lastDate: new HijriCalendar()
        ..hYear = 1500
        ..hMonth = 9
        ..hDay = 25,
      firstDate: new HijriCalendar()
        ..hYear = 1438
        ..hMonth = 12
        ..hDay = 25,
      initialDatePickerMode: DatePickerMode.day,
    );
    if (picked != null)
      setState(() {
        selectedDate = picked;
        controllerDateHegDebut.text = selectedDate.toString();
        controllerDateGregDebut.text = DateFormat("yyyy-MM-dd").format(
            HijriCalendar().hijriToGregorian(
                selectedDate.hYear, selectedDate.hMonth, selectedDate.hDay));
        // "${selectedDate.hYear}-${selectedDate.hMonth}-${selectedDate.hDay}";
      });
  }
}

Future<HijriCalendar?> showHijriDatePicker({
  required BuildContext context,
  required HijriCalendar initialDate,
  required HijriCalendar firstDate,
  required HijriCalendar lastDate,
  selectableDayPredicate,
  DatePickerMode initialDatePickerMode: DatePickerMode.day,
  Locale? locale,
  // TextDirection? textDirection,
}) async {
  assert(
      !initialDate.isBefore(firstDate.hYear, firstDate.hMonth, firstDate.hDay),
      'initialDate must be on or after firstDate');
  assert(!initialDate.isAfter(lastDate.hYear, lastDate.hMonth, lastDate.hDay),
      'initialDate must be on or before lastDate');
  assert(!firstDate.isAfter(lastDate.hYear, lastDate.hMonth, lastDate.hDay),
      'lastDate must be on or after firstDate');
  assert(selectableDayPredicate == null || selectableDayPredicate(initialDate),
      'Provided initialDate must satisfy provided selectableDayPredicate');

  Widget child = new HijriDatePickerDialog(
    initialDate: initialDate,
    firstDate: firstDate,
    lastDate: lastDate,
    selectableDayPredicate: selectableDayPredicate,
    initialDatePickerMode: initialDatePickerMode,
  );

  // if (textDirection != null) {
  //   child = new Directionality(
  //     textDirection: textDirection,
  //     child: child,
  //   );
  // }

  if (locale != null) {
    child = new Localizations.override(
      context: context,
      locale: locale,
      child: child,
    );
  }

  return showDialog<HijriCalendar>(
    context: context,
    builder: (BuildContext context) => child,
  );
}
