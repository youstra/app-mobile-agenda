import 'dart:ffi';
import 'dart:ui';
import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:intl/intl.dart';
import 'package:programme_dr_diallo/Categorie.dart';
import 'package:programme_dr_diallo/DetailsProgramme.dart';
import 'package:programme_dr_diallo/EditerProgramme.dart';
import 'package:programme_dr_diallo/constants.dart';
import 'package:programme_dr_diallo/main.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:flutter/material.dart';
import 'package:programme_dr_diallo/constants.dart';
import 'package:programme_dr_diallo/fonctions.dart';
import 'package:programme_dr_diallo/Programme.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);
  @override
  _HomePageSate createState() => _HomePageSate();
}

class _HomePageSate extends State<HomePage> {
  // List<Programme> programmes = Utilities.defaultProgrammes;
  List<Programme> programmes = [];

  @override
  void initState() {
    // getDixPremiersProgrammesAVenir();
    getDixPremiersProgrammesAVenirNew();
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    getDixPremiersProgrammesAVenirNew();
  }

  @override
  Widget build(BuildContext context) {
    // return Center(child: Text("Programmes"));
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Dix premiers programmes à venir',
              textScaleFactor: 1.5,
            ),
            Expanded(
              child: ListView.separated(
                  itemBuilder: (context, index) {
                    var value = programmes[index];
                    String dateActionToString =
                        DateFormat("dd-MM-yyyy").format(value.date!);
                    return ListTile(
                      onTap: () {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (BuildContext context) {
                          return DetailsProgramme(
                            programme: value,
                          );
                        }));
                      },
                      title: Text(
                        value.libelle ?? "",
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      trailing: Wrap(
                        // mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (BuildContext buildContext) {
                                  return EditerProgrammePage(programme: value);
                                }));
                              },
                              icon: Icon(Icons.edit)),
                          IconButton(
                            onPressed: () {
                              alert(value.id!);
                            },
                            icon: Icon(
                              Icons.delete,
                              color: Colors.red,
                            ),
                          ),
                        ],
                      ),
                      subtitle: Row(
                        children: [
                          Text((value.categorie?.libelle ?? "") + " le ",
                              style: TextStyle(fontStyle: FontStyle.italic)),
                          Text(
                            dateActionToString + " => ",
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(HijriCalendar().gregorianToHijri(
                              value.date!.year,
                              value.date!.month,
                              value.date!.day))
                        ],
                      ),
                      selected: true,
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider(
                      height: 2,
                      color: Colors.teal[700],
                      endIndent: 20,
                      indent: 20,
                    );
                  },
                  itemCount: programmes.length),
            )
          ],
        ),
      ),
    );
  }

  Future<Null> alert(int programmeId) async {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Confirmation de la suppression d\'un programme'),
            content: Text(
                'Etes-vous vraiment sûrs de bien vouloir supprimer ce programme ? Cette action est irreversible.'),
            contentPadding: EdgeInsets.all(20.0),
            actions: [
              TextButton(
                  onPressed: () {
                    print("Action annulée ${programmeId}");
                    Navigator.pop(context);
                  },
                  child: Text('Annuler',
                      style: TextStyle(color: Colors.grey[600]))),
              TextButton(
                onPressed: () {
                  print("Action acceptée ${programmeId}");
                  Navigator.pop(context);
                  deleteProgramme(programmeId);
                  setState(() {});
                },
                child: Text(
                  'Continuer',
                  style: TextStyle(color: Colors.red[500]),
                ),
              )
            ],
          );
        },
        barrierDismissible: false);
  }

  getDixPremiersProgrammesAVenirNew() async {
    var dio = Dio();
    try {
      var response = await dio
          // .get('http://127.0.0.1:8000/api/dix-premiers-programmes-a-venir');
          .get(
              "${basicUrl}dix-premiers-programmes-a-venir"); // il est mieux de mettre valeur dans un fichier constant ca evite les difficultes d'edition et les erreurs de saisie
      var itemsResponse = jsonDecode(response.data)["hydra:member"];
      for (var item in itemsResponse) {
        // var prog = Map<String, dynamic>.from(item);
        print(item["categorie"]["libelle"]);
        programmes.add(Programme(
            id: item["id"],
            categorie: Categorie(
                id: item["categorie"]["id"],
                libelle: item["categorie"]["libelle"]),
            date: DateTime.parse(item["date"]),
            lieu: item["lieu"],
            libelle: item["libelle"],
            commentaire: item["commentaire"],
            isDeleted: item["isDeleted"],
            heure: item["heure"]));
      }
      print('goooooog');
    } catch (e) {
      print(e);
    }
    this.setState(() {});
    //return programmesToReturn;
  }

  deleteProgramme(programmeId) async {
    print('deleteProgramme');
    var dio = Dio();
    try {
      var response = await dio.delete(
          "${basicUrl}programmes/${programmeId}"); // il est mieux de mettre valeur dans un fichier constant ca evite les difficultes d'edition et les erreurs de saisie
      print(
          "Retour suppression =======================================================");
      //print(response);
      print(response.statusCode);
      if (response.statusCode == 204) {
        print('new get');

        var dio = Dio();
        try {
          var response = await dio.get(
              "${basicUrl}dix-premiers-programmes-a-venir"); // il est mieux de mettre valeur dans un fichier constant ca evite les difficultes d'edition et les erreurs de saisie
          var itemsResponse = jsonDecode(response.data)["hydra:member"];

          List<Programme> listeProgrammes = [];
          for (var item in itemsResponse) {
            // var prog = Map<String, dynamic>.from(item);
            print(item["categorie"]["libelle"]);
            listeProgrammes.add(Programme(
                id: item["id"],
                categorie: Categorie(
                    id: item["categorie"]["id"],
                    libelle: item["categorie"]["libelle"]),
                date: DateTime.parse(item["date"]),
                lieu: item["lieu"],
                libelle: item["libelle"],
                commentaire: item["commentaire"],
                isDeleted: item["isDeleted"],
                heure: item["heure"]));
          }
          print('goooooog');
          programmes = listeProgrammes;
          this.setState(() {});
        } catch (e) {
          print(e);
        }
        //return programmesToReturn;

      }
    } catch (e) {
      print(e);
    }
  }

  // void getHttp() async {
  //   try {
  //     var response = await Dio().get('http://www.google.com');
  //     print(response);
  //   } catch (e) {
  //     print(e);
  //   }
  // }
}
